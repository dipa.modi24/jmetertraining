#!/bin/sh

sudo docker cp check_cpu.sh nagios:/usr/local/nagios/libexec/
sudo docker cp check_diskstat.sh nagios:/usr/local/nagios/libexec/
sudo docker cp check_mem.sh nagios:/usr/local/nagios/libexec/

sudo docker cp commands.cfg nagios:/usr/local/nagios/etc/objects/commands.cfg
sudo docker cp localhost.cfg nagios:/usr/local/nagios/etc/objects/localhost.cfg

sudo docker exec -it nagios /etc/init.d/nagios restart
